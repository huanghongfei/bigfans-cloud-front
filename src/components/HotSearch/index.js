/**
 * Created by lichong on 2/14/2018.
 */

import React from 'react';
import {Tag} from 'antd';

import {Link} from 'react-router-dom'

import HttpUtils from 'utils/HttpUtils';

class HotSearch extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount(){
        let self = this;
        HttpUtils.hotSearch({
            success(resp){
                self.setState({tagsFromServer : resp.data})
            },
            error(resp){

            }
        })
    }

    state = {
        tagsFromServer: []
    }

    render() {
        return (
            <div>
                <h6 style={{marginRight: 8, display: 'inline'}}>热门搜索:</h6>
                {this.state.tagsFromServer.map(tag => (
                    <Tag.CheckableTag
                        key={tag}
                    >
                        <Link to={"/search?q=" + tag}>{tag}</Link>
                    </Tag.CheckableTag>
                ))}
            </div>
        );
    }
}

export default HotSearch;
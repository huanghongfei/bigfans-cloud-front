/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';
import {Row, Col , Divider} from 'antd';

import HttpUtils from '../../utils/HttpUtils'

class AttributesTable extends React.Component {

    state = {
        attributes: []
    }

    componentDidMount() {
        let self = this;
        HttpUtils.getProductAttributes({params: {prodId: this.props.prodId}}, {
            success: function (resp) {
                self.setState({attributes: resp.data});
            }
        })
    }

    render() {
        return (
            <div>
                <h3>规格与包装</h3>
                <Divider/>
                <Row>
                    {
                        this.state.attributes.map((attr) => {
                            return (
                                <Row gutter={20}>
                                    <Col span={8}>
                                        <span className='pull-right'>
                                            {attr.optionName}
                                        </span>
                                    </Col>
                                    <Col span={16}>
                                        <span className='left'>
                                            {attr.value}
                                        </span>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </Row>
            </div>
        );
    }
}

export default AttributesTable;
/**
 * Created by lichong on 3/9/2018.
 */

import React from 'react';
import { Card, Button, Dropdown, Menu , Spin } from 'antd';

import ProductItem from '../ProductItem'

const gridStyle = {
    width: '25%',
    textAlign: 'center',
    padding : '10px'
};

class MyFavors extends React.Component {

    state = {
        products : []
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <Card bordered={false}>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                    <Card.Grid style={gridStyle}><ProductItem data={this.state.products} addProductToCart={this.props.addProductToCart}/></Card.Grid>
                </Card>
            </div>
        );
    }
}

export default MyFavors;
/**
 * Created by lichong on 2/27/2018.
 */
import React, {Component} from 'react';

import {Row, Col, Card, Divider} from 'antd';
import TopBar from '../../components/TopBar';
import FootBar from '../../components/FootBar';

import HttpUtils from '../../utils/HttpUtils'

class Pay extends Component {

    state = {
        payment: {},
        order: {},
        qrImage: ''
    }

    componentDidMount() {
        let self = this;
        let orderId = HttpUtils.getQueryString("orderId");
        HttpUtils.getQrImage({params: {orderId: orderId}}, {
            success (resp) {
                self.setState({qrImage: resp.data})
                self.startCheck(orderId)
            },
            error (resp) {

            }
        })

        HttpUtils.getOrder({id: orderId}, {
            success(resp){
                if (resp.data) {
                    self.setState({order: resp.data})
                }
            },
            error(resp){

            }
        })
    }

    startCheck(orderId){
        let self = this;
        setInterval(function () {
            HttpUtils.checkPaymentStatus(orderId, {
                success(resp){
                    if(resp.data === true){
                        self.props.history.push('/myorders');
                    }
                },
                error(){

                }
            })
        } , 2000)
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row>
                            <span>请您在提交订单后24小时内完成支付，否则订单会自动取消。</span>
                        </Row>
                        <Row>
                            <span className="pull-left">订单提交成功，请您尽快付款！ 订单号：{this.state.order.id}</span>
                            <span className="pull-right">应付金额<span style={{color:'red'}}>{this.state.order.totalPrice}</span>元</span>
                        </Row>
                        <Divider/>
                        <Row>
                            <Col span={2}>
                                收货地址：
                            </Col>
                            <Col span={22}>
                                {this.state.order.addressDetail}
                            </Col>
                            <Col span={2}>
                                收货人：
                            </Col>
                            <Col span={22}>
                                {this.state.order.addressConsignee} {this.state.order.mobile}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={2}>
                                商品名称：
                            </Col>
                            <Col span={22}>
                                {
                                    this.state.order.items
                                    &&
                                    this.state.order.items.map((item , index) => {
                                        return (
                                            <div key={index}>
                                                <span>{item.prodName} {item.specs}</span>
                                            </div>
                                        )
                                    })
                                }
                            </Col>
                        </Row>
                        <Divider/>
                        <Row>
                            <Card>
                                <img src={this.state.qrImage}/>
                            </Card>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <FootBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default Pay;
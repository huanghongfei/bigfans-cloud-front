# 简介
这个项目是bigfans-cloud https://gitee.com/dafanshudl/bigfans-cloud 的前端门户

# 技术栈
React 16.2.0

Ant Design 3.2.1

# 运行步骤
1. npm install
2. npm start

# 效果图
看这里  :point_right:  https://gitee.com/dafanshudl/bigfans-cloud


[![star](https://gitee.com/dafanshudl/bigfans-cloud-front/badge/star.svg?theme=dark)](https://gitee.com/dafanshudl/bigfans-cloud-front/stargazers)